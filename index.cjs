const dropdown_toggle = document.querySelector(".dropdown-toggle");
const navigation = document.querySelector(".header-ul");

dropdown_toggle.addEventListener("click", () => {
  if (navigation.classList.contains("active")) {
    navigation.classList.remove("active");
  } else {
    navigation.classList.add("active");
  }
});
